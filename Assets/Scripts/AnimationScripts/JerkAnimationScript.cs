﻿using UnityEngine;
using System.Collections;

public class JerkAnimationScript : StateMachineBehaviour {

    // Менять из Animator (анимация Jerk)
    public float jerkForce = 30f;
    public float jerkTime = .3f;
    public StateMachine stateMachine;

    private float m_durationJerk;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        m_durationJerk = Time.time + jerkTime;
        stateMachine.CRigidbody.AddForce(new Vector2(jerkForce * stateMachine.direction, 0), ForceMode2D.Impulse);
        //stateMachine.CRigidbody.velocity = new Vector2(30, stateMachine.CRigidbody.velocity.y);
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (Time.time > m_durationJerk)
        {
            stateMachine.IsJerk = false;
        }
        //stateMachine.SetState(stateMachine.GetJerkState(), stateMachine.GetIdleState());
        //stateMachine.jerkPlayer(30 * 1);   
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { 
    //    
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
