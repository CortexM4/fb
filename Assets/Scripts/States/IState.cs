﻿//using UnityEngine;
//using System.Collections;

public interface IState
{
	float m_duration { get; set;}			// Current time of action
	float actionTime { get; }				// Time before action disabled. Initialized in constructor of state

    /* Level 1 State*/
    void stopPlayer();
    void movePlayer(float moveSpeed);
    void jumpPlayer(float jumpForce);
	void blockPlayer();
    void attackOne();
    void attackTwo();
	
    /* Level 2 State */
    void jerkPlayer(float force);

    /* Special Ability */
    void specialAbility();
}

