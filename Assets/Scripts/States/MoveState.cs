﻿using UnityEngine;

public class MoveState : IState {

	public float m_duration { get; set; }

	// Max duration of action for MoveState
	public float actionTime 
	{ 
		get
		{
			return .1F;			
		}
	}

    StateMachine stateMachine;
    private float lastTime = 0;
    private float lastDirection =  0;

    public MoveState(StateMachine stateMachine)
    {				
		this.stateMachine = stateMachine;
		m_duration = 0;
    }

    public void stopPlayer()
    {
        stateMachine.IsMoving = false;
        stateMachine.CRigidbody.velocity = Vector2.zero;
		stateMachine.SetState(stateMachine.GetIdleState(), this);
        /* Анимация Idle */
        /* Эта анимация тут не нужна, оставлю в коменте на всякий случай */
        //stateMachine.AnimatorChar.SetBool("IsGrounded", true);
    }

    public void movePlayer(float moveSpeed)
    {
		//m_duration += Time.deltaTime;
        if (!stateMachine.IsMoving)                 // Если мы сюда попадаем, то переход в состояние Move только-только произошел
        {
            float currTime = Time.time;
            // Минимальная задержка между двумя нажатиями для перехода в состояние Jerk 
            // Направление должно совпадать с предыдущим
            if ((currTime - lastTime) < .2f && lastDirection == stateMachine.direction) 
            {
                stateMachine.IsJerk = true;
                //stateMachine.CRigidbody.velocity = Vector2.zero;
                stateMachine.SetState(stateMachine.GetJerkState(), this);
            }
            lastDirection = stateMachine.direction;
            lastTime = currTime;
        }

        stateMachine.IsMoving = true;

        /* Физика движения */
        stateMachine.CRigidbody.velocity = new Vector2(moveSpeed * stateMachine.direction, stateMachine.CRigidbody.velocity.y);

    }

    public void jumpPlayer(float jumpForce)
    {
        //stateMachine.IsMoving = false;
        stateMachine.CRigidbody.AddForce(new Vector2(0, jumpForce));
		stateMachine.SetState(stateMachine.GetJumpState(), this);
    }


    public void jerkPlayer(float force)
    {
        throw new System.NotImplementedException();
    }

	public void blockPlayer()
	{
		//throw new System.NotImplementedException();
        stateMachine.SetState(stateMachine.GetBlockState(), this);
	}


    public void attackOne()
    {
        stateMachine.CRigidbody.velocity = Vector2.zero;
        stateMachine.IsAttackOne = true;
        stateMachine.SetState(stateMachine.GetAttackOneState(), this);
    }


    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    public void attackTwo()
    {
        stateMachine.CRigidbody.velocity = Vector2.zero;
        stateMachine.IsAttackTwo = true;
        stateMachine.SetState(stateMachine.GetAttackTwoState(), this);
    }
}
