﻿using System;
using UnityEngine;

public class AttackOneState : IState {

	public float m_duration { get; set; }
	// Max duration of action for JumpState
	public float actionTime 
	{ 
		get
		{
			return .35f;			
		}
	}

    StateMachine stateMachine;

    public AttackOneState(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
    }

    public void stopPlayer()
    {
        //Debug.Log(m_duration);
        //m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
        stateMachine.IsMoving = false;                          // Этот флаг надо сбрасывать, т.к. при быстром нажати Move&Attack персонаж продолжает двигать ногами хотя должен стоять
        //if (m_duration > actionTime)
        if (!stateMachine.IsAttackOne)
        {
            //stateMachine.IsAttackOne = false;
            stateMachine.SetState(stateMachine.GetIdleState(), this);
        }
    }

    public void movePlayer(float moveSpeed)
    {
        //m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
         //if (m_duration > actionTime)
         if(!stateMachine.IsAttackOne)
         {
             stateMachine.IsMoving = true;
             stateMachine.SetState(stateMachine.GetMoveState(), this);
         }
    }

    public void jumpPlayer(float jumpForce)
    {
        throw new System.NotImplementedException();
    }


    public void jerkPlayer(float force)
    {
        throw new System.NotImplementedException();
    }

	public void blockPlayer()
	{
        //m_duration += Time.deltaTime;
        //if (m_duration > actionTime)
        if (!stateMachine.IsAttackOne)
        {
            //stateMachine.IsAttackOne = false;
            stateMachine.SetState(stateMachine.GetBlockState(), this);
        }
        //if (!stateMachine.IsAttackOne)
        //    stateMachine.SetState(stateMachine.GetBlockState(), this);
    }

    public void attackOne()
    {
       /* m_duration += Time.deltaTime;
        if (m_duration > actionTime)
        {
            stateMachine.IsAttackOne = true;
            m_duration = 0;
            stateMachine.SetState(stateMachine.GetAttackOneState(), this);
        }*/
    }


    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    public void attackTwo()
    {
        stateMachine.IsAttackTwo = true;
        stateMachine.SetState(stateMachine.GetAttackTwoState(), this);
    }
}
