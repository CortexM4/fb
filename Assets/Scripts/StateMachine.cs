﻿using UnityEngine;

public class StateMachine  {

    /*
     *  Флаги управления анимацией в аниматоре.
     */
    public bool isGrounded { get; set; }
    public bool IsAttackOne { get; set; }
    public bool IsAttackTwo { get; set; }
    public bool IsJerk { get; set; }
    public bool IsMoving { get; set; }

    /* Level 1 */
    private IState m_movePlayerState;
    private IState m_idlePlayerState;
    private IState m_jumpPlayerState;
	private IState m_blockPlayerState;
    private IState m_attackOnePlayerState;
    private IState m_attackTwoPlayerState;

    /* Level 2 */
    private IState m_jerkPlayerState;

    /* Special Ability */
    private IState m_specialAbilityPlayerState;

    private IState m_state;
    private IState m_lastState;

    private Animator m_anima;
    public Animator AnimatorChar 
    {
        get { return m_anima; }
    }

    private Rigidbody2D m_Rigidbody;
    public Rigidbody2D CRigidbody
    {
        get { return m_Rigidbody; }
    }

	public float Damage { get; set; }

    public int direction;

    public StateMachine(Rigidbody2D m_Rigidbody, Animator m_anima)
    {
        m_movePlayerState = new MoveState(this);
        m_jumpPlayerState = new JumpState(this);
        m_idlePlayerState = new IdleState(this);
        m_jerkPlayerState = new JerkState(this);
		m_blockPlayerState = new BlockState(this);
        m_attackOnePlayerState = new AttackOneState(this);
        m_attackTwoPlayerState = new AttackTwoState(this);

        m_state = m_idlePlayerState;
        m_lastState = m_idlePlayerState;

        this.m_anima = m_anima;
        this.m_Rigidbody = m_Rigidbody;

        IsMoving = false;
        IsJerk = false;
    }

    /*
     *  Level 1 State (Move, Jump, Stay, AttackOne) 
     */
	public void stopPlayer() 
	{
        if (!(m_state is IdleState))
            Debug.Log(m_state + "-->Stop");
		m_state.stopPlayer();
	}

	public void movePlayer(float moveSpeed)
	{
        Debug.Log(m_state + "-->Move");
		m_state.movePlayer(moveSpeed);
	}

	public void jumpPlayer(float jumpForce)
	{
        Debug.Log(m_state + "-->Jump");
		m_state.jumpPlayer(jumpForce);
	}

	public void blockPlayer()
	{
		Debug.Log(m_state + "-->Block");
		m_state.blockPlayer();
	}

    public void attackOne()
    {
		Debug.Log(m_state + "-->Attack One");
        m_state.attackOne();
    }

    public void attackTwo()
    {
        Debug.Log(m_state + "-->Attack Two");
        m_state.attackTwo();
    }

    /*
     *  Level 2 State (Jerk)
     */

    public void jerkPlayer(float force)
    {
        Debug.Log(m_state + "-->Jerk");
        m_state.jerkPlayer(force);
    }

    /*
     *  Speical Ability
     */
    public void specialAbility()
    {
        Debug.Log(m_state + "--> Special Ability");
        m_state.specialAbility();
    }


    /*
     *  Service functions
     */

    public void SetState(IState state, IState lastState)
    {
        m_lastState = lastState;
        state.m_duration = 0;
        m_state = state;
    }

    public IState GetLastState() { return m_lastState; }

    public IState GetIdleState() { return m_idlePlayerState; }
    public IState GetMoveState() { return m_movePlayerState; }
    public IState GetJumpState() { return m_jumpPlayerState; }
    public IState GetAttackOneState() { return m_attackOnePlayerState; }
    public IState GetAttackTwoState() { return m_attackTwoPlayerState; }
    public IState GetBlockState() { return m_blockPlayerState; }

    public IState GetJerkState() { return m_jerkPlayerState; }

    public IState GetSpecialAbilityState() { return m_specialAbilityPlayerState; }
    public void SetSpecialAbility(IState specialAbility) { m_specialAbilityPlayerState = specialAbility; }
}
